package com.hello.HelloWeb;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "proizvod", path = "proizvod")
public interface ProizvodRepo extends PagingAndSortingRepository<Proizvod , Long> {

    List<Proizvod> imeProizvod(@Param("ime") String imeProizvod);
}






