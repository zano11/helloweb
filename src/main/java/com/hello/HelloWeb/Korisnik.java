package com.hello.HelloWeb;

import  javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Korisnik {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;


    private String email;
    private String ime;
    private String prezime;
    private String telefon;
    private String grad;

    /*public Korisnik(int id, String email, String ime, String prezime, String telefon , String grad) {
        this.id=id;
        this.email=email;
        this.ime = ime;
        this.prezime = prezime;
        this.telefon = telefon;
        this.grad = grad;
    }
    public Korisnik(){}
*/
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getGrad() {
        return grad;
    }

    public void setGrad(String grad) {
        grad = grad;
    }



}
