package com.hello.HelloWeb;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Proizvod {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;


    private String imeProizvod;
    private int cenaProizvod;
    private String kategorija;
    private String vremeNaIzrabotka;
    private String grad;

 /*   public Proizvod(){}

    public Proizvod(String imeProizvod, int cenaProizvod, String kategorija, String vremeNaIzrabotka){
        this.imeProizvod= imeProizvod;
        this.cenaProizvod = cenaProizvod;
        this.kategorija = kategorija;
        this.vremeNaIzrabotka = vremeNaIzrabotka;
    }

*/

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImeProizvod() {
        return imeProizvod;
    }

    public void setImeProizvod(String imeProizvod) {
        this.imeProizvod = imeProizvod;
    }

    public int getCenaProizvod() {
        return cenaProizvod;
    }

    public void setCenaProizvod(int cenaProizvod) {
        this.cenaProizvod = cenaProizvod;
    }

    public String getKategorija() {
        return kategorija;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }

    public String getVremeNaIzrabotka() {
        return vremeNaIzrabotka;
    }

    public void setVremeNaIzrabotka(String vremeNaIzrabotka) {
        this.vremeNaIzrabotka = vremeNaIzrabotka;
    }

    public String getGrad() {
        return grad;
    }

    public void setGrad(String grad) {
        this.grad = grad;
    }
}
