package com.hello.HelloWeb;


import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "korisnik", path = "korisnik")
public interface KorisnikRepo extends PagingAndSortingRepository<Korisnik, Long> {

    List<Korisnik> ime(@Param("ime") String ime);

}


